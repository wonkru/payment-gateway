<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'merchantId' => "tommyfrench",
    'accountId'=> "internet",
    'merchantSecret' => "secret",
    'hppUrl' => "https://pay.sandbox.realexpayments.com/pay",
    'currency' => "GBP",
];
