<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
?>


<div class="container ">
    <div class="wrapper-content">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <span style="font-size:14px; color:#fff;">Please enter your details below in order to deposit with us. If you do not have an account already, please call us before making any deposits. 028 3832 2420</span>

            </div>
        </div>
    </div>
</div>
        
<div class="container ">
    <div class="wrapper-content">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="well well-sm">
               
                 <?php $form = ActiveForm::begin(["action" => Url::to(["/payment/data-post"],true), "method" => "post", "id" => "payment-form"]); ?>                                    
                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="heading">
                                    Payment Detail
                                </h3>
                                <div class="form-group">
                                    <label for="name">
                                        Name
                                    </label>
                                    <input class="form-control" id="Name" name="Name" type="text" value="" required>     
                                </div>

                                <div class="form-group">
                                    <label for="Email">
                                        Email(Optional)
                                    </label>
                                    <input class="form-control" id="Email" name="Email" type="email" value="">

                                </div>

                                <div class="form-group">
                                    <label for="Phone">
                                        Phone Number(Optional)
                                    </label>
                                    <input class="form-control" id="PhoneNumber" name="PhoneNumber" type="number" value="">
                                    

                                </div>
                                <div class="form-group">
                                    <label for="AccountReference">
                                        Account Reference
                                    </label>
                                    <input class="form-control" id="ORDER_ID" name="ORDER_ID" type="text" value="" required>         

                                </div>
                                <div class="form-group">
                                    <label for="totalamount">
                                        Total Amount(in whole pounds only e.g. 25.00)
                                    </label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <span class="">£</span>
                                        </span>
                                        <input class="form-control" id="AMOUNT" name="AMOUNT" type="text" value="0">
                                        
                                    </div>
                                </div>
                                <input id="AccountRefference" name="AccountRefference" type="hidden" value="">
                                <input id="CURRENCY" name="CURRENCY" type="hidden" value="EUR">
                                <input id="SHA1HASH" name="SHA1HASH" type="hidden" value="">
                                <input id="AUTO_SETTLE_FLAG" name="AUTO_SETTLE_FLAG" type="hidden" value="1">
                                <input id="TIMESTAMP" name="TIMESTAMP" type="hidden" value="">
                                
                                <div class="form-group">
                                    <label for="comments">
                                        Comments
                                    </label>

                                    <textarea class="form-control" cols="25" id="Comments" name="Comments" rows="9"></textarea>
                                </div>

                            </div>

                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary pull-right" id="btnPaynow">
                                    Pay Now
                                </button>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>                              
               
                </div>
            </div>

        </div>
    </div>
</div>
