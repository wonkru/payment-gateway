<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Redirecting';
?>

<div class="main-page form-page page-min-height page-pt page-pb page-mb-n">
        <div class="container">
           <div class="row">
            <div class="col-lg-6 col-md-8 offset-lg-3 offset-md-2 pt-2 pt-md-3">
              <div class="card mt-3 mb-4">
                <div class="card-header">
                  <h3>Message</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                      <h3 style="margin-left: 30px">Page redirecting to payment.</h3>
                   </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/dist/rxp-js.js"></script>
<script>
    RealexHpp.setHppUrl('https://pay.sandbox.realexpayments.com/pay');
    // get the HPP JSON from the server-side SDK
    // $(document).ready(function () {
        $.post("/payment/data-post", function (jsonFromServerSdk) {
            RealexHpp.redirect.init("btnPaynow","https://dev.rlxcarts.com/mobileSDKsV2/response.php", jsonFromServerSdk);
            $('body').addClass('loaded');
        });
    // });
</script>
