<?php

use yii\helpers\Url;

?>
                    
<div class="container" style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);">
    <div class="wrapper-content">
        <div class="row justify-content-center">
            <div class="col-md-6 col-md-offset-3">
                <div>  
                    <?php if($sucess == true) { ?>
                        <div class="text-center alert alert-success p-20">
                            <p style="font-size:14px; line-height: 1.5em; font-weight: 400; margin-bottom: 15px">
                             <strong>Successful – The transaction has been authorized successfully.</strong></p>
                            <a href="http://www.tommybet.co.uk/" class="btn btn-success mr-1 mt-3">
                              <span>Ok</span>
                            </a>    
                        </div>
                        
                    <?php } else{ ?> 
                        <div class="text-center alert alert-danger p-20">
                            <p style="font-size:14px; line-height: 1.5em; font-weight: 400;">Error : <?= str_replace("[ test system ] ","",$parsedResponse->responseMessage) ?></p>
                            <a href="http://www.tommybet.co.uk/" class="btn btn-danger mr-1 mt-3 ">
                              <span>Ok</span>
                            </a>   
                        </div>
                         
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</div>
          