<?php

use yii\helpers\Url;

$this->title = 'Tommy French';
?>
<div class="row">
  <div class="col-md-4 col-md-offset-4">
    <button class="btn-primary btn hide" type="submit" id="payButtonId" >Checkout</button>
  </div>
</div>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/js/rxp-js.js',['depends' => [yii\web\JqueryAsset::className()]]); ?>
<?php
$hppUrl = Yii::$app->params["hppUrl"];
$redirectUrl = Url::to(["/payment/process-payment", 'id' => $model->id],true);
$script = <<< JS
   RealexHpp.setHppUrl('$hppUrl');
    $(document).ready(function () {
            console.log("loaded", RealexHpp.lightbox);
            RealexHpp.lightbox.init("payButtonId", "$redirectUrl", $data);

            console.log( $('#payButtonId'));
            $('body').addClass('loaded');
            $('#payButtonId').trigger('click');
    });
JS;

$this->registerJs($script);
?>

