<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="header-version-2 BodyBackground   pace-done pace-done">
<?php $this->beginBody() ?>
       <div class="pace  pace-inactive pace-inactive">
            <div class="pace-progress" style="width: 100%;" data-progress-text="100%" data-progress="99">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div>
        </div>
        <div class="pace pace-active">
            <div class="pace-progress" style="width: 0%;" data-progress-text="0%" data-progress="00">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div>
        </div>

        <div class="navbar megamenu NavBarTop" role="navigation">
            <!--Info Top-->
            
            <!--Branding-->
            <div id="BrandingNav" class="NavBar-Branding">
                <div class="container NavBar-BrandingContainer">
                    <div class="navbar-header">
                        <a class="navbar-brand " href="http://www.tommybet.co.uk/"> 
                            <img src="<?=Url::to('@web/images/TopNav-TF.png')?>" alt="Tommy French">
                        </a>
                        
                    </div>
                    <div class="nav navbar-nav navbar-right hidden-xs">
                    </div>
                </div>
            </div>
        </div>
        <!-- BODY -->
        <div id="MainContainer" data-currentcontroller="Home" class="container">
          <?= $content ?>
       </div>

<!-- </div> -->

     <div class="footer-bottom">
            <div class="container">
                <p class="pull-left"> © Tommy French Bookmakers <?=date('Y')?> All right reserved. </p>
            </div>
            <p></p>
        </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
