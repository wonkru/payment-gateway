<?php

namespace app\controllers;

use Yii;
use app\models\Transaction;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use GlobalPayments\Api\ServiceConfigs\Gateways\GpEcomConfig;
use GlobalPayments\Api\Services\HostedService;
use GlobalPayments\Api\Entities\Exceptions\ApiException;


class PaymentController extends Controller
{   
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                	[
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'data-post' => ['post'],
                    'process-payment' => ['post']
                ],
            ],
        ];
    }

	public function beforeAction($action)
	{            
	    if ($action->id == 'process-payment') {
	        $this->enableCsrfValidation = false;
	    }

	    return parent::beforeAction($action);
	}

	 /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
    	//echo "uyu"; die;
        return $this->render('index');
    }

    public function actionDataPost()
    {
    	$post = Yii::$app->request->post();
    	if(isset($post) && !empty($post)){
    		$model = new Transaction();
    		$model->name = $post['Name'];
    		$model->email = $post['Email'];
    		$model->phonenumber = $post['PhoneNumber'];
    		$model->order_id = $post['ORDER_ID'];
    		$model->amount = $post['AMOUNT'];
    		$model->accountrefference = $post['ORDER_ID'];
    		$model->currency = Yii::$app->params["currency"];
    		$model->auto_settle_flag ="1";
    		$model->comments = $post['Comments'];

    		if ($model->save()) {
    			$data =[
						  "MERCHANT_ID" =>Yii::$app->params["merchantId"],
						  "ACCOUNT"=> Yii::$app->params["accountId"],
						  "AMOUNT"=> $model->amount,
						  "CURRENCY"=>$model->currency,
						  "AUTO_SETTLE_FLAG"=>$model->auto_settle_flag,
						  "PM_METHODS"=>"cards"
						];
    			$data["ORDER_ID"] = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz0123456789'), 0, 22);
				$data["TIMESTAMP"] = date("YmdHis");
				$data["SHA1HASH"] = $this->generateHash($data, Yii::$app->params["merchantSecret"]);
	            return $this->render('card-holder',['model' => $model, 'data' => json_encode($data)]);
	        }
    	}
    	return $this->render('status',['sucess' => false, 'parsedResponse' => (object) ['responseMessage' => 'Invalid Data']]);
    }

    public function actionProcessPayment($id=0){
    	//echo"test"; die;
    	$config = new GpEcomConfig();
		$config->merchantId = Yii::$app->params["merchantId"];
		$config->accountId = Yii::$app->params["accountId"];
		$config->sharedSecret = Yii::$app->params["merchantSecret"];
		$config->serviceUrl = Yii::$app->params["hppUrl"];

		$service = new HostedService($config);

		$post = Yii::$app->request->post();
		$sucess = false;
		$responseJson = $post['hppResponse'];
		try {
		   $parsedResponse = $service->parseResponse($responseJson, true);
		   $model = Transaction::findOne($id);
		   //print_r($parsedResponse->responseCode); die;
		   if(($model) && ($parsedResponse->responseCode == 00)){
		   	$model->status = $parsedResponse->responseMessage;
		   	$model->order_id = $parsedResponse->responseValues['ORDER_ID'];
		   	$model->sha1hash = $parsedResponse->responseValues['SHA1HASH'];
		   	$model->save();
		   	$sucess = true;
		   }
		} catch (ApiException $e) {
		
		}

		return $this->render('status',['sucess' => $sucess, 'parsedResponse' => $parsedResponse]);
    }

    private function generateHash($data, $secret)
	{

	    $toHash = [];
	    $timeStamp           = !isset($data['TIMESTAMP']) ? "" : $data['TIMESTAMP'];
	    $merchantId          = !isset($data['MERCHANT_ID']) ? "" : $data['MERCHANT_ID'];
	    $orderId             = !isset($data['ORDER_ID']) ? "" : $data['ORDER_ID'];
	    $amount              = !isset($data['AMOUNT']) ? "" : $data['AMOUNT'];
	    $currency            = !isset($data['CURRENCY']) ? "" : $data['CURRENCY'];
	    $payerReference      = !isset($data['PAYER_REF']) ? "" : $data['PAYER_REF'];
	    $paymentReference    = !isset($data['PMT_REF']) ? "" : $data['PMT_REF'];
	    $hppSelectStoredCard = !isset($data['HPP_SELECT_STORED_CARD']) ? "" : $data['HPP_SELECT_STORED_CARD'];
	    $payRefORStoredCard  = empty($hppSelectStoredCard) ?  $payerReference : $hppSelectStoredCard;

	    if (isset($data['CARD_STORAGE_ENABLE']) && $data['CARD_STORAGE_ENABLE'] === '1') {
	        $toHash = [
	            $timeStamp,
	            $merchantId,
	            $orderId,
	            $amount,
	            $currency,
	            $payerReference,
	            $paymentReference,
	        ];
	    } elseif ($payRefORStoredCard && empty($paymentReference)) {
	        $toHash = [
	            $timeStamp,
	            $merchantId,
	            $orderId,
	            $amount,
	            $currency,
	            $payRefORStoredCard,
	            ""
	        ];
	    } elseif ($payRefORStoredCard && !empty($paymentReference)) {
	        $toHash = [
	            $timeStamp,
	            $merchantId,
	            $orderId,
	            $amount,
	            $currency,
	            $payRefORStoredCard,
	            $paymentReference,
	        ];
	    } else {
	        $toHash = [
	            $timeStamp,
	            $merchantId,
	            $orderId,
	            $amount,
	            $currency,
	        ];
	    }

	    return sha1(sha1(implode('.', $toHash)) . '.' . $secret);
	}
}
