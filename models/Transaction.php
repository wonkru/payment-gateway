<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Transaction".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phonenumber
 * @property int $order_id
 * @property float $amount
 * @property int $accountrefference
 * @property string $currency
 * @property string $sha1hash
 * @property int $auto_settle_flag
 * @property string $comments
 * @property string $created_at
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'order_id'], 'required'],
            [['id'], 'integer'],
            [['amount'], 'number'],
            [['created_at'], 'safe'],
            [['name', 'email', 'sha1hash', 'comments', 'status', 'order_id', 'auto_settle_flag', 'accountrefference'], 'string', 'max' => 255],
            [['phonenumber'], 'string', 'max' => 16],
            [['currency'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phonenumber' => 'Phonenumber',
            'order_id' => 'Order ID',
            'amount' => 'Amount',
            'accountrefference' => 'Accountrefference',
            'currency' => 'Currency',
            'sha1hash' => 'Sha1hash',
            'auto_settle_flag' => 'Auto Settle Flag',
            'comments' => 'Comments',
            'created_at' => 'Created At',
        ];
    }
}
